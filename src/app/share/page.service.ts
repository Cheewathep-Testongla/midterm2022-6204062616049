import { MockData } from './mock-data';
import { Flight } from './../flight';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  Data: Flight[] = [];

  constructor() {
    this.Data = MockData.ExampleData;
  }

  getData(): Flight[] {
    return this.Data;
  }

  addData(d:Flight) : void {
    this.Data.push(d);
  }
}
